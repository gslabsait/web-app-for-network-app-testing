import httplib2
import json
import sys
import logging



baseUrl = 'http://192.168.56.101:8080/controller/nb/v2'
containerName = 'default/'

def post_dict(h, url, d):
  resp, content = h.request(
      uri = url,
      method = 'PUT',
      headers={'Content-Type' : 'application/json'},
      body=json.dumps(d)
    )
  return resp, content

def post_flow(nodeid, new_flow, flowname):
    req_str = baseUrl + '/flowprogrammer/default/node/OF/' + nodeid + '/staticFlow/' + flowname    
    logging.debug('req_str %s', req_str)
    resp, content = post_dict(h, req_str, new_flow)
    logging.debug('resp %s', resp)
    logging.debug('content %s', content)

def build_flow(nodeid, flowname, ethertype='', destip='', ipcos='', ipprot='', 
            installflag='', outnodeconn='', outdstmac='', vlan='', innodeconn=''):
    newflow = {}
    
    newflow['name'] = flowname
    if (installflag != ''):
        newflow['installInHw'] = installflag
    else:
        newflow['installInHw'] = 'true'
    newflow['node'] = {u'id': nodeid, u'type': u'OF'}
    if (destip != ''):
        newflow['nwDst'] = destip
    if (ethertype != ''):
        newflow['etherType'] = ethertype
    if (ipcos != ''):
        newflow['tosBits'] = ipcos
    if (ipprot != ''):
        newflow['protocol'] = ipprot
    if (vlan != ''):
        newflow['vlanId'] = vlan
    if (innodeconn != ''):
        newflow['ingressPort'] = innodeconn
    newflow['priority']=500
    node = {}
    node['id'] = nodeid
    node['type'] = 'OF'
    newflow['node'] = node
    
#    actions1 = 'OUTPUT='+str(outnodeconn)
#    if (outdstmac != ''):
#        actions2 = 'SET_DL_DST='+str(outdstmac)
#    else:
#        actions2 = ''
#    logging.debug('actions1 %s actions2 %s',actions1, actions2)
    newflow['actions'] = ['DROP']
    return newflow

    


LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

if len(sys.argv) > 1:
    level_name = sys.argv[1]
    level = LEVELS.get(level_name, logging.NOTSET)
    logging.basicConfig(level=level)

h = httplib2.Http(".cache")
h.add_credentials('admin', 'admin')
    
if __name__ == "__main__":
        
    def flow_add():
        print 'Adding flow'
        install = 'true'
        nodeid='00:00:00:00:00:00:00:02'
        nodeconnector=2
        ether_type=0x800
        dst_mac='00:00:00:00:00:02	'
        dst_ip='10.0.0.2'
        fname='test'
        new_flow = build_flow(nodeid=nodeid, flowname=fname, outnodeconn=nodeconnector, ethertype=ether_type, 
                        installflag = install, destip=dst_ip, outdstmac=dst_mac)
        print 'new_flow', new_flow
        post_flow(nodeid, new_flow, fname)
    

    flow_add()
  